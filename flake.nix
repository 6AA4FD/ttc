{

	inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
	};

	outputs = { self, nixpkgs, nix, ... }:
	let

		supportedSystems = [ "x86_64-linux" ];
		forAllSystems = f: nixpkgs.lib.genAttrs supportedSystems (system: f system);

	in {

		overlay = final: prev: {

				ttc = with final; with python37Packages; buildPythonApplication rec {

					name = "ttc";
					nativeBuildInputs = [ fzf youtube-dl mpv ];
					propagatedBuildInputs = [
						twitchAPI
						pprintpp
						toml
					];
					src = ./.;

				};

				twitchAPI = with final; with python37Packages; buildPythonPackage rec {

					version = "2.2.0";
					pname = "twitchAPI";

					src = fetchPypi {

						inherit pname version;
						sha256 = "ukgi/YD30UmB1gF89WOxREFyEOcnJJVXGMjkSWgVlRg=";

					};

					propagatedBuildInputs = [ aiohttp python-dateutil websockets requests ];

				};

		};

		packages = forAllSystems (system: let
				pkgs = import nixpkgs {
					inherit system;
					overlays = [ self.overlay ];
				};
			in {
				ttc = pkgs.ttc;
				twitchAPI = pkgs.twitchAPI;
		});

	};

}

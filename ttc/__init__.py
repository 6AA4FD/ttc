from twitchAPI.twitch import Twitch
from pprintpp import pprint as pp

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("config", help="the toml configuration file you want to use")
parser.add_argument("-f", "--print_followed", help="print all the channels which are both listed under followed_users and currently broadcasting", action="store_true")
parser.add_argument("-g", "--game_search", help="search for a game by name", type=str, action="append")
parser.add_argument("-l", "--games_live", help="show live streams for a certain game by nickname defined in your config file", type=str, action="append")
parser.add_argument("-p", "--print_only", help="don't open streams, just print them", action="store_true")

import toml
import subprocess
import sys

def main():
    parser.parse_args()
    args = parser.parse_args()
    cfgf = (open(args.config))
    cfg = toml.loads(cfgf.read())
    cfgf.close
    twitch = Twitch(cfg['app_key'], cfg['app_secret'])
    twitch.authenticate_app([])
    if args.game_search:
        print_id(twitch, cfg, args)
    if args.games_live:
        show_game(twitch, cfg, args)
    if args.print_followed:
        show_follows(twitch, cfg, args)

def show_follows(twitch, cfg, args):
    livenow_ = twitch.get_streams(language=cfg['language'], user_login=cfg['followed_users'], first=10)['data']
    streams_prompt(livenow_, args)

def print_id(twitch, cfg, args):
    pp(twitch.get_games(names=args.game_search)['data'])

def show_game(twitch, cfg, args):
    game_ids = []
    for x in args.games_live:
        game_ids.append(cfg['games'][x])
    livenow_ = twitch.get_streams(language=cfg['language'], game_id=game_ids, first=10)['data']
    streams_prompt(livenow_, args)

def streams_prompt(livenow_, args):
    if args.print_only:
        pp(livenow_)
        return
    livenow = ''
    for a in livenow_:
        livenow += 'http://twitch.tv/' + a['user_name'].replace('\n', " ") + ', ' + a['title'].replace('\n', " ") + ', ' + a['game_name'].replace('\n', " ") + '\n'
    fzf = subprocess.run(['fzf', '--multi=5'], stdout=subprocess.PIPE, input=livenow, text=True)
    selection = fzf.stdout.splitlines()[0].partition(',')[0]
    subprocess.run(["mpv", selection])
